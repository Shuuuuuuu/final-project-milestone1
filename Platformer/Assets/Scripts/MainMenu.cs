﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    private string stringLevel;

    public void Level(string _Level)
    {
        stringLevel = _Level;
        SceneManager.LoadScene(stringLevel);
    }

    public void Quit()
    {
        Application.Quit();
        Debug.Log("Game Quit");
    }
}
